package ru.kharlamova.tm.repository;

import ru.kharlamova.tm.api.repository.IProjectRepository;
import ru.kharlamova.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public List<Project> findAll(Comparator<Project> comparator) {
        final List<Project> projects = new ArrayList<>(entities);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public Project findOneByIndex(final String userId, final Integer index) {
        return entities.get(index);
    }

    @Override
    public Project findOneByName(final String userId, final String name) {
        for (final Project project: entities) {
            if (!userId.equals(project.getUserId())) continue;
            if(name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project removeOneByIndex(final String userId, final Integer index) {
        final Project project = findOneByIndex(userId, index);
        if (userId == null) return null;
        if (project == null) return null;
        entities.remove(project);
        return project;
    }

    @Override
    public Project removeOneByName(final String userId, final String name) {
        final Project project = findOneByName(userId, name);
        if (userId == null) return null;
        if (project == null) return null;
        entities.remove(project);
        return project;
    }

}
