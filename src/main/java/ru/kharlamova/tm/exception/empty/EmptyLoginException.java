package ru.kharlamova.tm.exception.empty;

import ru.kharlamova.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error! Login is empty.");
    }

}
