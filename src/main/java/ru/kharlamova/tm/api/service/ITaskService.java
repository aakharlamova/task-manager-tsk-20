package ru.kharlamova.tm.api.service;

import ru.kharlamova.tm.api.IService;
import ru.kharlamova.tm.enumerated.Status;
import ru.kharlamova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IService<Task> {

    List<Task> findAll(Comparator<Task> comparator);

    Task add(String userId, String name, String description);

    Task findOneByName(String userId, String name);

    Task removeOneByName(String userId, String name);

    Task findOneByIndex(String userId, Integer index);

    Task removeOneByIndex(String userId, Integer index);

    Task updateTaskByIndex(String userId, Integer index, String name, String description);

    Task updateTaskById(String userId, String id, String name, String description);

    Task startTaskById(String userId, String id);

    Task startTaskByIndex(String userId, Integer index);

    Task startTaskByName(String userId, String name);

    Task finishTaskById(String userId, String id);

    Task finishTaskByIndex(String userId, Integer index);

    Task finishTaskByName(String userId, String name);

    Task changeTaskStatusById(String userId, String id, Status status);

    Task changeTaskStatusByIndex(String userId, Integer index, Status status);

    Task changeTaskStatusByName(String userId, String name, Status status);

}
