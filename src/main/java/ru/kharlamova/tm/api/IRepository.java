package ru.kharlamova.tm.api;

import ru.kharlamova.tm.model.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    List<E> findAll(String userId);

    E add(String userId, E entity);

    E findById(String userId, String id);

    void clear(String userId);

    E removeById(String userId, String id);

    void remove(String userId, E entity);

}
